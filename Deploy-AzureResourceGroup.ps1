#region [#### Variables declariation]
$arrSubscriptionName               = "Func"
$arrLocation                       = "Westeurope"
$arrResourceGroupName              = "arrlvmmngtdisk"

$basedir = ""
$ErrorActionPreference = "SilentlyContinue"
$basedir = split-path -parent $MyInvocation.MyCommand.Path -ErrorAction SilentlyContinue
if ($basedir -eq "") {$basedir = split-path -parent $psISE.CurrentFile.FullPath}
$ErrorActionPreference = "Continue"

$arrTemplateName                   = "$basedir\template.json"
$arrTemplateParameters             = "$basedir\parameters.json"
$arrExtTemplateName                = "$basedir\custScriptExtension.json"
$arrExtTemplateParameters          = "$basedir\custScriptExtension.parameters.json"
$masterTemplateFile                = "$basedir\masterTemplate.json"
$masterTemplateParametersFile      = "$basedir\masterTemplate.parameters.json"
$nsgRuleTemplate                   = "$basedir\nsgRuleTemplate.json"
$nsgRulePortIpTemplate             = "$basedir\nsgRulePortIpTemplate.json"


#endregion

#region [#### Login to Azure, set the Context and working directory]
# Checks if the login to Azure has already been done
try
{
    Get-AzureRmContext | out-null
} # use the code "$Error[0] | fl * -Force" to find the exception to catch
catch [System.Management.Automation.PSInvalidOperationException]
{
    Write-Host "Not Logged in"
    $loggedIn = $false
}
finally
{
    If (!$loggedIn) {
        Login-AzureRmAccount
        $loggedIn = $true
    }
    $accountId = (Get-AzureRmContext).Account.id
    Write-Host "You are logged in to Azure as: $accountId"
}

## Set the Azure Context
Set-AzureRmContext -SubscriptionName $arrSubscriptionName
Set-Location $basedir
#endregion

#region [#### Create the Resource Group - forces the recreation if the RG already exists]
# Create or update a resource group for this specific project 
New-AzureRmResourceGroup -Name $arrResourceGroupName `
                         -Location $arrLocation `
                         -Force `
                         -ErrorAction Stop `
                         -Verbose 
#endregion

#region [#### Launch VM Deployment]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem "$arrTemplateName").BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $arrTemplateName `
                                   -TemplateParameterFile $arrTemplateParameters `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $arrTemplateName  `
#                                    -TemplateParameterFile $arrTemplateParameters    

#endregion

#region [#### Launch VM Extension Deployment]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $arrExtTemplateName).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $arrExtTemplateName `
                                   -TemplateParameterFile $arrExtTemplateParameters `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $arrExtTemplateName `
#                                    -TemplateParameterFile $arrExtTemplateParameters    

#endregion

#region [#### Launch the Master Template]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $masterTemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $masterTemplateFile `
                                   -TemplateParameterFile $masterTemplateParametersFile `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $masterTemplateFile `
#                                    -TemplateParameterFile $masterTemplateParametersFile `
#                                    -verbose

#endregion

#region [#### Launch the NSG Template]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $nsgRuleTemplate).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $nsgRuleTemplate  `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $nsgRuleTemplate).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $nsgRuleTemplate  `
                                   -destinationPortRange "180" `
                                   -sourceAddressPrefix "79.66.212.85" `
                                   -priority "3010" `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $nsgRuleTemplate 

#endregion

#region [#### Launch the NSG IP and Port Template]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $nsgRulePortIpTemplate).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $nsgRulePortIpTemplate  `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $nsgRulePortIpTemplate 

#endregion

#region [#### Remove the Resource Group]
#Remove-AzurermResourceGroup -Name $arrResourceGroupName -Force
#endregion
