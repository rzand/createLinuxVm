#region [#### Variables declariation]
$arrSubscriptionName               = "Func"
$arrLocation                       = "Westeurope"
$arrResourceGroupName              = "arrlvmmngtdisk"

$basedir = ""
$ErrorActionPreference = "SilentlyContinue"
$basedir = split-path -parent $MyInvocation.MyCommand.Path -ErrorAction SilentlyContinue
if ($basedir -eq "") {$basedir = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(�.\�)}
$ErrorActionPreference = "Continue"

$arrTemplateName                   = "$basedir\template.json"
$arrTemplateParameters             = "$basedir\parameters.json"
$arrExtTemplateName                = "$basedir\custScriptExtension.json"
$arrExtTemplateParameters          = "$basedir\custScriptExtension.parameters.json"
$masterTemplateFile                = "$basedir\masterTemplate.json"
$masterTemplateParametersFile      = "$basedir\masterTemplate.parameters.json"
#endregion

#region [#### Login to Azure, set the Context and working directory]
# Checks if the login to Azure has already been done
try
{
    Get-AzureRmContext | out-null
} # use the code "$Error[0] | fl * -Force" to find the exception to catch
catch [System.Management.Automation.PSInvalidOperationException]
{
    Write-Host "Not Logged in"
    $loggedIn = $false
}
finally
{
    If (!$loggedIn) {
        Login-AzureRmAccount
        $loggedIn = $true
    }
    $accountId = (Get-AzureRmContext).Account.id
    Write-Host "You are logged in to Azure as: $accountId"
}

## Set the Azure Context
Set-AzureRmContext -SubscriptionName $arrSubscriptionName
Set-Location $basedir
#endregion

#region [#### Create the Resource Group - forces the recreation if the RG already exists]
# Create or update a resource group for this specific project 
New-AzureRmResourceGroup -Name $arrResourceGroupName `
                         -Location $arrLocation `
                         -Force `
                         -ErrorAction Stop `
                         -Verbose 
#endregion

#region [#### Launch VM Deployment]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem "$arrTemplateName").BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $arrTemplateName `
                                   -TemplateParameterFile $arrTemplateParameters `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $arrTemplateName  `
#                                    -TemplateParameterFile $arrTemplateParameters    

#endregion

#region [#### Launch VM Extension Deployment]

New-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
                                   -Name ((Get-ChildItem $arrExtTemplateName).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                   -TemplateFile $arrExtTemplateName `
                                   -TemplateParameterFile $arrExtTemplateParameters `
                                   -Mode Incremental `
                                   -Force `
                                   -Verbose

#Test-AzureRmResourceGroupDeployment -ResourceGroupName $arrResourceGroupName `
#                                    -TemplateFile $arrExtTemplateName `
#                                    -TemplateParameterFile $arrExtTemplateParameters    

#endregion

#region [#### Remove the Resource Group]
#Remove-AzurermResourceGroup -Name $arrResourceGroupName -Force
#endregion



